﻿﻿<?php 
    $pageTitle ="Contact Us | ";
    include("header.php")
?>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main class="page-content" style="min-height: 100%;">

        <!--  -->
        <section class="bg-secondary-lightest well-md text-center text-md-left">
            <div class="container">
                <h2>Contact Form</h2>
                <!-- RD Mailform -->
                <form class='rd-mailform form-offset' method="post" action="https://aalei.ph/bat/rd-mailform.php">
                    <!-- RD Mailform Type -->
                    <input type="hidden" name="form-type" value="contact"/>
                    <!-- END RD Mailform Type -->
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label" data-add-placeholder for="mailform-input-name">Enter your name</label>
                                    <input id="mailform-input-name"
                                           type="text"
                                           name="name"
                                           data-constraints="@NotEmpty @LettersOnly"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label" data-add-placeholder for="mailform-input-email">Enter your e-mail</label>
                                    <input id="mailform-input-email"
                                            type="text"
                                           name="email"
                                           data-constraints="@NotEmpty @Email"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label" data-add-placeholder for="mailform-input-phone">Your telephone here:</label>
                                    <input id="mailform-input-phone"
                                            type="text"
                                           name="phone"
                                           data-constraints="@Phone"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group textarea">
                            <label class="form-label" data-add-placeholder
                                   for="mailform-input-textarea">Message</label>
                      <textarea id="mailform-input-textarea"
                                name="message"
                                data-constraints="@NotEmpty"></textarea>

                        </div>

                        <div class="mfControls btn-group text-center text-md-left">
                            <button class="btn btn-md btn-primary" type="submit"><span>Submit</span></button>
                        </div>

                        <div class="mfInfo"></div>
                    </fieldset>
                </form>
                <!-- END RD Mailform -->
            </div>
        </section>
        <!-- END -->

        <!-- RD Google Map -->
        <div class="rd-google-map">
            <div id="map" class="rd-google-map__model" data-zoom="14" data-x="14.572036" data-y="120.983755"></div>
            <ul class="rd-google-map__locations">
                <li data-x="14.572036" data-y="120.983755">
                    <p class="text-center">9870 St Vincent Place, Glasgow, <br>DC 45 Fr 45. <span>800 2345-6789</span></p>
                </li>
            </ul>
            <div class="contact-info bg-primary-darken text-center text-md-left">
                    <h3>Contact Info</h3>
                    <address>201-202 Vareb Mansion, Mabin St.<br class="hidden visible-md-block">
                        cor Malvar St. Malate, Manila</address>
                    <dl class="max-width">
                        <dt>Telephone No.:</dt><dd><a href='callto:(02) 310-3219'>(02) 310-3219</a></dd>
                        <dt>Mobile No.:</dt><dd><a href='callto:+63 933 866 3341'>+63 933 866 3341</a></dd>
                        <!-- <dt>Telephone: </dt><dd><a href='callto:#'>+1 123 456 7890</a></dd> -->
                        <!-- <dt>FAX:</dt><dd><a href='callto:#'>+1 123 456 7890</a></dd> -->
                        <dt class="width-auto">E-mail:&nbsp;</dt><dd>
                            <ul class="unstyle-list">
                                <li><a href='mailto:admin@aalei.ph' class="text-underline">admin@aalei.ph</a></li>
                                <li><a href='mailto:account@aalei.ph' class="text-underline">account@aalei.ph</a></li>
                            </ul>
                        </dd>
                    </dl>
            </div>
        </div>
        <!-- END RD Google Map -->

    </main>

<?php 
    include("footer.php")
?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASfbeTFCShzITG7mHMGRoXdzwOCUBrtV4"></script>
<script type="text/javascript">

            // The latitude and longitude of your business / place
            var position = [14.572036,120.983755];
            function showGoogleMaps() {
             
                var latLng = new google.maps.LatLng(position[0], position[1]);
             
                var mapOptions = {
                    zoom: 14, // initialize zoom level - the max value is 21
                    streetViewControl: true, // hide the yellow Street View pegman
                    scaleControl: false, // allow users to zoom the Google Map
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: new google.maps.LatLng(14.572036,120.983755)
                    // styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
            
                };
             
                map = new google.maps.Map(document.getElementById('map'), mapOptions);
            
                // Show the default red marker at the location
                var mapIcon = 'images/gmap_marker_active.png';
                marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    draggable: false,
                    title: 'Hi there!',
                    icon: mapIcon,
                    animation: google.maps.Animation.DROP
                });
            }
            google.maps.event.addDomListener(window, 'load', showGoogleMaps);
        </script>

</script>
